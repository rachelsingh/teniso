# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame

class Ludanto:
	def __init__( self, playerNumber ):
		self.m_dimensioj = [ 10, 50 ]
		
		self.m_koloro = pygame.Color( 255, 255, 255 )
		
		self.m_koordinatoj 	= [ 0, 0 ]
		self.m_koordinatoj[1] = 480/2 - self.m_dimensioj[1]/2
		
		self.m_rapideco = 0
		self.m_poentaro 	= 0
		
		self.m_suprenKlavo 	= pygame.K_UP
		self.m_subenKlavo 	= pygame.K_DOWN
		
		if ( playerNumber == 1 ):
			self.m_koordinatoj[0] = 10
			self.m_koloro = pygame.Color( 200, 100, 0 )
		else:
			self.m_koordinatoj[0] = 640 - 10 - self.m_dimensioj[0]
			self.m_koloro = pygame.Color( 0, 100, 200 )
			self.m_suprenKlavo = pygame.K_w
			self.m_subenKlavo = pygame.K_s
	# __init__ 
	
	def Movi( self, klavoj ):
		# Player  Velocity adjustment
		if 		( klavoj[ self.m_suprenKlavo ] ):
			self.m_rapideco -= 1	
		elif 	( klavoj[ self.m_subenKlavo ] ):
			self.m_rapideco += 1
		else:
			if ( self.m_rapideco < 0 ):
				self.m_rapideco += 1
			elif ( self.m_rapideco > 0 ):
				self.m_rapideco -= 1
			
		# Move player 
		self.m_koordinatoj[1] += self.m_rapideco
		
		# Adjust coordinates
		if ( self.m_koordinatoj[1] < 0 ):
			self.m_koordinatoj[1] = 0
			self.m_rapideco = -self.m_rapideco
		elif ( self.m_koordinatoj[1] + self.m_dimensioj[1] > 480 ):
			self.m_koordinatoj[1] = 480 - self.m_dimensioj[1]
			self.m_rapideco = -self.m_rapideco
	# Move
	
	def Vidigi( self, fenestro ):
		rect = ( self.m_koordinatoj[0], self.m_koordinatoj[1], self.m_dimensioj[0], self.m_dimensioj[1] )
		pygame.draw.rect( fenestro, self.m_koloro, rect )
	# Draw
	
	def PreniPoentaro( self ):
		return self.m_poentaro
	# PreniPoentaro
	
	def AldoniAlPoentaro( self ):
		self.m_poentaro += 1
	# AldoniAlPoentaro
# Player
