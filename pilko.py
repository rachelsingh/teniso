# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame

class Pilko:
	def __init__( self ):
		self.m_dimensioj = [ 25, 25 ]
		
		self.m_koloro = pygame.Color( 255, 255, 255 )
		
		self.m_koordinatoj 	= [ 0, 0 ]
		self.Resxargu( 1 )
	# __init__ 
	
	def Resxargu( self, lundantoNombro ):
		self.m_koordinatoj[0] = 640/2 - self.m_dimensioj[0]/2
		self.m_koordinatoj[1] = 480/2 - self.m_dimensioj[1]/2
		
		if ( lundantoNombro == 1 ):
			self.m_rapideco = [ 2, 0 ]
		else:
			self.m_rapideco = [ -2, 0 ]
	# Resxargu
	
	def CxuBatas( self, player ):
		if ( 	self.m_koordinatoj[0]					< player.m_koordinatoj[0] + player.m_dimensioj[0] and
				self.m_koordinatoj[0] + self.m_dimensioj[0] > player.m_koordinatoj[0] and
				self.m_koordinatoj[1]					< player.m_koordinatoj[1] + player.m_dimensioj[1] and
				self.m_koordinatoj[1] + self.m_dimensioj[1] > player.m_koordinatoj[1] ):
			# Collision
			
			self.m_rapideco[0] = -self.m_rapideco[0]
			
			if ( self.m_rapideco[0] < 0 ):
				self.m_rapideco[0] -= 1
			elif ( self.m_rapideco[0] > 0 ):
				self.m_rapideco[0] += 1
			
			# What part of the paddle was hit?
			myCenterY 		= self.m_koordinatoj[1] + self.m_dimensioj[1]/2
			paddleCenterY 	= player.m_koordinatoj[1] + player.m_dimensioj[1]/2
			
			if ( myCenterY > paddleCenterY ):
				self.m_rapideco[1] += 2
			elif ( myCenterY < paddleCenterY ):
				self.m_rapideco[1] -= 2
			else:
				self.m_rapideco[1] = 0
		
		
	# CxuBatas
	
	def Movi( self ):			
		# Move player 
		self.m_koordinatoj[0] += self.m_rapideco[0]
		self.m_koordinatoj[1] += self.m_rapideco[1]
		
		# Adjust coordinates
		if ( self.m_koordinatoj[1] < 0 ):
			self.m_koordinatoj[1] = 0
			self.m_rapideco[1] = -self.m_rapideco[1]
		elif ( self.m_koordinatoj[1] + self.m_dimensioj[1] > 480 ):
			self.m_koordinatoj[1] = 480 - self.m_dimensioj[1]
			self.m_rapideco[1] = -self.m_rapideco[1]
	# Move
	
	def CxuLundanto2Poentas( self ):
		return ( self.m_koordinatoj[0] + self.m_dimensioj[0] < 0 )
	# CxuLundanto2Poentas
	
	def CxuLundanto1Poentas( self ):
		return ( self.m_koordinatoj[0] > 640 )
	# CxuLundanto1Poentas
	
	def Vidigi( self, fenestro ):
		rect = ( self.m_koordinatoj[0], self.m_koordinatoj[1], self.m_dimensioj[0], self.m_dimensioj[1] )
		pygame.draw.rect( fenestro, self.m_koloro, rect )
	# Draw
	
	def PreniPoentaro( self ):
		return self.m_poentaro
	# PreniPoentaro
# Player
