# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame
import sys

from ludanto import Ludanto
from pilko import Pilko

pygame.init()
fenestro = pygame.display.set_mode( ( 640, 480 ) )

fontObj = pygame.font.Font( "fonts/Averia-Bold.ttf", 24 )
fpsTempo = pygame.time.Clock()

ludantoUnu = Ludanto( 1 )
ludantoDu = Ludanto( 2 )

pilko = Pilko()

while True:
	for okazo in pygame.event.get():
		if ( okazo.type == pygame.QUIT ):
			pygame.quit()
			sys.exit()
	# for okazo
	
	klavoj = pygame.key.get_pressed()
	ludantoUnu.Movi( klavoj )
	ludantoDu.Movi( klavoj )
	
	# Pilko movement
	pilko.Movi()
	pilko.CxuBatas( ludantoUnu )
	pilko.CxuBatas( ludantoDu )
	
	if ( pilko.CxuLundanto2Poentas() ):
		pilko.Resxargu( 1 )
		ludantoUnu.AldoniAlPoentaro()
	elif ( pilko.CxuLundanto1Poentas() ):
		pilko.Resxargu( 2 )
		ludantoDu.AldoniAlPoentaro()
	
	pygame.display.update()
	fpsTempo.tick( 60 )
	
	# Draw
	fenestro.fill( pygame.Color( 50, 50, 50 ) )

	ludantoUnu.Vidigi( fenestro )
	ludantoDu.Vidigi( fenestro )
	pilko.Vidigi( fenestro )
	
	poentaro1teksto = fontObj.render( "Ludanto 1: " + str( ludantoUnu.PreniPoentaro() ), False, pygame.Color( 200, 100, 0 ) )
	poentaro2teksto = fontObj.render( "Ludanto 2: " + str( ludantoDu.PreniPoentaro() ), False, pygame.Color( 0, 100, 200 ) )
	
	poentaro1rektangulo = poentaro1teksto.get_rect()
	poentaro2rektangulo = poentaro2teksto.get_rect()
	
	poentaro1rektangulo.x = 10
	poentaro2rektangulo.x = 640 - 170
	poentaro1rektangulo.y = 10
	poentaro2rektangulo.y = 10
	
	fenestro.blit( poentaro1teksto, poentaro1rektangulo )
	fenestro.blit( poentaro2teksto, poentaro2rektangulo )
	
	titoloTeksto = fontObj.render( "Teniso", False, pygame.Color( 255, 255, 255 ) )
	titoloRektangulo = titoloTeksto.get_rect()
	titoloRektangulo.x = 640/2 - 40
	titoloRektangulo.y = 480 - 50
	
	fenestro.blit( titoloTeksto, titoloRektangulo )
	
# while True
